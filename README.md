UKAIR
=====

UKAIR is the Flask (flask.pocoo.org) application for the
https://asselect.uk/ web site.

Contributing
------------

I'm Alan Sparrow

UKAIR is on [GitLab](https://gitlab.com/ahsparrow/ukair).

Please get in touch, via GitLab or otherwise. If you've got something
to contribute it would be very welcome.
